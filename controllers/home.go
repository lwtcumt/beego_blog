package controllers

import(
	"github.com/astaxie/beego"
	"myapp/models"
)

type HomeController  struct {
	beego.Controller
}
func (this *HomeController) Get(){
	//this.Ctx.WriteString("hello world")
	this.Data["IsHome"] = true
	this.TplName = "home.html"

	this.Data["IsLogin"] = checkAccount(this.Ctx)
	topics,err := models.GetAllTopics(this.Input().Get("cate"),this.Input().Get("label"),true)


	if err !=nil{
		beego.Error(err)
	}else{
		this.Data["Topics"] = topics
	}

	category,err := models.GetAllCategories()

	if err !=nil{
		beego.Error(err)
	}else{
		this.Data["Category"] = category
	}
}

