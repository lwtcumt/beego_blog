package controllers

import (
	"github.com/astaxie/beego"
	"net/url"
	"os"
	"io"
)

type AttachController struct{
	beego.Controller
}

func (this *AttachController) Get()  {
	filePath,err := url.QueryUnescape(this.Ctx.Request.RequestURI[1:])  // 外面 是编码的  去掉 /fsadfasdfasdf/fdsfasdf.jpg  去掉第一个 /

	if err != nil{
		this.Ctx.WriteString(err.Error())
		return
	}


	f ,err :=os.Open(filePath)
	if err != nil{
		this.Ctx.WriteString(err.Error())
	}

	defer f.Close()

	_,err =io.Copy(this.Ctx.ResponseWriter,f)   //  1 目的地   2 源

	if err != nil{
		this.Ctx.WriteString(err.Error())
	}
}
