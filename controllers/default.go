package controllers

import (
	"github.com/astaxie/beego"

	"github.com/beego/i18n"
)

type baseController struct{
	beego.Controller
	i18n.Locale

}


func (this *baseController) Prepare(){
	lang := this.GetString("lang")

	if lang == "zn-CN"{
		this.Lang = lang
	}else{
		this.Lang = "en-US"  // 默认 赋值
	}


	this.Data["Lang"] = this.Lang // 不用小lang  是因为 可能 lang  不合法 用默认  然后还要 加入 模板函数
 }

type DefaultController struct {
	baseController
}

func (c *DefaultController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "liwotian@lingjiang"
	c.TplName = "index.html"

	// 语言测试

	c.Data["testHi"] = "hi"
	c.Data["testBye"] = "bye"

	// 在main.go  注册过之后  可以用 Tr 方法 而不是函数 来
	c.Data["test2Hi"] = c.Tr("hi")
	c.Data["test2Bye"] = c.Tr("bye")


	//语言同词分区

	c.Data["testAbout"] = "about"
	c.Data["test2About"] = ".about."  // 第一个点 就是 歧义处理 处理 about.这个特殊 的  键



	c.Data["TrueCond"] =true;
	c.Data["FalseCond"] =false;  // 大小写都行




	type u struct {
		Name string
		Age int
		Sex string
	}


	user := &u{   // 穿件一个
		Name:"joe",
		Age:20,
		Sex:"男",

	}

	c.Data["user"] = user
	nums :=[]int{1,2,3,4,5,6,7,8,9,0}

	c.Data["nums"] = nums

	//模板变量
	c.Data["TpVar"] = "hi  guys"


	//高级点的


	//模板函数  注册成为模板函数

	c.Data["myhtml"] ="<div>hello liwotian </div>"


	c.Data["Pipe"] = "<div>   hi mygo </div>"



}
