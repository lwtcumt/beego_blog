package controllers

import(
	"myapp/models"
	"github.com/astaxie/beego"
	"fmt"
)


type ReplyController struct {
	beego.Controller
}

func (c *ReplyController) Add(){
	tid := c.Input().Get("tid")
	tid2 := c.GetString("tid")

	fmt.Println("tid")
	err := models.AddReply( tid,c.Input().Get("nickname"),c.Input().Get("content"))

	if err != nil{
		beego.Error(err)
	}

	c.Redirect("/topic/view/id/"+tid2,302)

}


func (this *ReplyController) Delete(){


	if !checkAccount(this.Ctx){
		this.Redirect("/login",302)
		return
	}
	tid := this.GetString("tid")
	tid = this.Input().Get("tid")

	err := models.DelReply(this.Input().Get("rid"));

	if err != nil{
		beego.Error(err)
	}

	this.Redirect("/topic/view/id/"+tid,302)
	return

}