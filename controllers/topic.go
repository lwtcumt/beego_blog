package controllers

import (
	"github.com/astaxie/beego"
	"myapp/models"
	"fmt"
	"strings"
	"path"
)

type TopicController struct {
	beego.Controller
}

func ( c *TopicController) Get(){

	op := c.Input().Get("op")
	switch op {
	case "del":
		if !checkAccount(c.Ctx){
			c.Redirect("/login",302)

			return
		}
		id :=c.Input().Get("id")
		if len(id)==0{
			break
		}
		err := models.DelTopic(id)

		if err != nil{
			beego.Error(err)
		}

		c.Redirect("/topic",302)
		return
	}





	if checkAccount(c.Ctx){
		c.Data["IsLogin"]=true
	}
		c.Data["IsTopic"] = true
		c.TplName="topic.html"

		topics,err :=models.GetAllTopics( c.Input().Get("cate"),c.Input().Get("label"),false)
		if err !=nil{
			beego.Error(err)
		}else{
			c.Data["Topics"]=topics
		}


}

func  (this * TopicController) Post(){
	fmt.Printf("eeeeeeeeeeeeeeeeeeeeee")

	//this.Ctx.WriteString("33333333333333333")
	if !checkAccount(this.Ctx){
		this.Redirect("/login",302)

		return
	}
	//解析表单
	title := this.Input().Get("title")
	content := this.Input().Get("content")
	category := this.Input().Get("category")
	label := this.Input().Get("label")
	tid := this.Input().Get("tid")


	//判断是否有文件上
	_,fh,err:= this.GetFile("fujian") // name ="fujian"

	if err!= nil{
		beego.Error(err)
	}

	var attachmentname string // 万一 没有 用这个顶着  变量要存在

	if fh !=nil{  //filehand
		//保存附件

		attachmentname = fh.Filename
		fmt.Print("888888888888->")
		fmt.Print(attachmentname)
		fmt.Print("<888888888888->")
		beego.Info(attachmentname)  // 打印一下 后台 了解下
		fmt.Print("<999999->")
		err = this.SaveToFile("fujian",path.Join("attachment",attachmentname))//二参数  不一定使用绝对目录 可以使 相对目录   path.join   filename


		if  err != nil{
			beego.Error(err)
		}
	}




	if len(tid) ==0{

		err = models.AddTopic(title,category,label,content,attachmentname)
	}else{
		fmt.Print("hahahahahaahahahahaahahahahahaahah")
		err = models.ModifyTopic(title,category,label,content,tid,attachmentname)
	}

	if err != nil{
		beego.Error(err)
	}

	this.Redirect("/topic",302)

}
func (this * TopicController) Add(){
	//this.Ctx.WriteString("++++++=")

	this.TplName="topic_add.html"
}


func (this * TopicController) View(){

	id := this.Ctx.Input.Param("1") // 获取除了 controller/method  后第一个  /id/22  id ->0  22->1
	topicinfo ,err := models.GetTopicinfo(id)

	if err!= nil{
		beego.Error(err)
		this.Redirect("/",302)
	}
	this.Data["Topicinfo"] = topicinfo;
	this.Data["Tid"] = id;
	this.Data["Labels"] = strings.Split(topicinfo.Labels," ");

	this.TplName="topic_view.html"
	this.Data["IsLogin"] = checkAccount(this.Ctx)

	replies,err := models.GetAllReplies(id)

	if err !=nil{
		beego.Error(err)
		return
	}

	this.Data["Replies"] = replies;
}



func (c * TopicController) Modify(){
	tid := c.Input().Get("tid")

	topicinfo ,err := models.GetTopicinfo(tid)

	if err != nil{
		beego.Error(err)
		c.Redirect("/topic",301)
	}
	c.Data["Topicinfo"] = topicinfo
	c.Data["Tid"] = tid

	c.TplName ="topic_modify.html"

}

//
//func (this * TopicController) One(){
//	mystruct :={...}
//	this.Data["json"] = &mystruct
//	this.ServeJSON()
//}



