package main

import (
	"myapp/models"
	_ "myapp/routers"
	"github.com/astaxie/beego"

	"github.com/astaxie/beego/orm"
	"fmt"
	"time"
	"github.com/beego/i18n"
)

func init()  {
	models.RegisterDB()//初始化

	fmt.Println("初始化数据库+1")

}
func main() {

	orm.Debug = true // 调试模式打开
	orm.RunSyncdb("default",false,true)// 二参 如果 true  每次 一直重建  数据重新

	// 注册 语言国际化  方法  两个参数
	i18n.SetMessage("en-US","conf/locale_en-US.ini")
	i18n.SetMessage("zn-CN","conf/locale_zh-CN.ini")

	beego.AddFuncMap("mydateform",mydateform)  // 自己定义一个模板函数


	// 增加 一个 lang 的  模板 函数

	beego.AddFuncMap("i18n_wotian",i18n.Tr)  // 此处Tr  是函数 不是 方法  三参输入

	beego.Run()
}

func mydateform(times int64 )(out string){

	tm2 := time.Unix(times, 0)
	out = tm2.Format("2006-01-02 03:04:05 PM")
	return
}

