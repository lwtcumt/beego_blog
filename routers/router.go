package routers

import (
	"myapp/controllers"
	"github.com/astaxie/beego"
	"os"
)

func init() {
	beego.Router("/",&controllers.HomeController{})
	beego.Router("/t",&controllers.DefaultController{})
	beego.Router("/temp",&controllers.TempController{})
	beego.Router("/login",&controllers.LoginController{})
	beego.Router("/category",&controllers.CategoryController{})
	beego.Router("/topic",&controllers.TopicController{})
	beego.Router("/reply",&controllers.ReplyController{})
	beego.Router("/reply/add",&controllers.ReplyController{},"post:Add")
	beego.Router("/reply/del",&controllers.ReplyController{},"get:Delete")
	beego.AutoRouter(&controllers.TopicController{})//topic 自动路由  需要 Controller



	//  创建附件menu
	os.Mkdir("attachment",os.ModePerm)

	//第一种附件方式  作为静态文件
	//beego.SetStaticPath("/attachment","attachment")  //  定义静态目录

	//第二种 作为单独一个控制器来处理

	beego.Router("/attachment/:all",&controllers.AttachController{})







}
